+++
title = "Seting up Oracle 11G Database in linux using Docker"
date = 2023-10-11
draft=false
[taxonomies]
tags=["oracle database in docker"]
+++


-------

## Installing Docker

Make sure you have installed Docker in your Linux system. Run `docker --version` 
if output is similar to `Docker version 24.0.5, build ced0996600`, then docker is installed <br> 
else, you can follow [Docker Installatiion Guide](https://docs.docker.com/desktop/install/linux-install/) to install.



## Downloading Installation Files 

Download Oracle Database 11gR2 installation files from the official [Oracle Site](https://www.oracle.com/database/technologies/oracle-database-software-downloads.html).
Installation files are: `linux.x64_11gR2_database_1of2.zip` and `linux.x64_11gR2_database_2of2.zip` 



## Prepare Directory Structure

Create a directory `db_files` to organize the database installation binaries. You can structure the directory as below 

```bash 
mkdir ~/oracle/db_files -p
mv linux.x64_11gR2_database_1of2.zip ~/oracle/db_files 
mv linux.x64_11gR2_database_2of2.zip ~/oracle/db_files 
cd ~/oracle/db_files
unzip linux.x64_11gR2_database_1of2.zip
unzip linux.x64_11gR2_database_2of2.zip
```



## Pull the Docker image and run the Docker Container

To pull the Docker image use below command
> docker pull jaspeen/oracle-11g

To run the container use below command
```bash
docker run -p 1521:1521 -p 8080:8080 --name oracle-db \
      -e ORACLE_ALLOW_REMOTE=true \
      -e INSTALL_DB=true \
      -v /home/<your_username>/oracle/:/install \
      jaspeen/oracle-11g:latest
```


NOTE: put your username inside `<your_username>`

Wait for few minutes to install the database 



## Connecting to the Oracle Database

After container is up and running, you can connect to the Oracle Database using a tool like SQL*Plus or Oracle SQL Developer. 
Use the following details to connect:
```
Host: 127.0.0.1 ( if docker is running locally) or <IP Address of remote Linux machine>
Port: 1521
SID: oracl
Username: system
Password: oracle (default-password)
```

To connect to database using **SQL*Plus** use following command:
> sqlplus system/oracle@//localhost:1521/orcl



## Change the default Password

After connecting, use the follow SQL command to change the password for the `SYSTEM` user. You need to specify the new password in single or double quotes

> ``` ALTER USER SYSTEM IDENTIFIED BY "<new_password>"; ```


For example:
> ``` ALTER USER SYSTEM IDENTIFIED BY "passw0rd"; ```


Commit the change after running "ALTER USER" statement with the following SQL statement

```sql
COMMIT; 
EXIT;
```



## Connecting with new password using `sqlplus` CLI utility

Use the following command to connect to the Oracle Database using SQL*Plus

```
sqlplus <username>/<password>@//<hostname-or-IP>:1521/<service-name>
```

example: 
```
sqlplus system/passw0rd@//locahost:1521/orcl
```



## Connecting with SQL Developer (Oracle provided tool)

### installing SQL Developer
Download SQL Developer ```sqldeveloper-*-no-jre.zip``` from [SQL Developer Downloads](https://www.oracle.com/database/sqldeveloper/technologies/download/)

Unzip the archive and follow as below:
> unzip sqldeveloper*.zip -d ~/oracle/sqldeveloper

Insert below configuration in `~/.local/share/applications/sqldeveloper.desktop`
```
[Desktop Entry]
Name=SQL Developer
Exec=/home/<your_username>/Downloads/oracle/sqldeveloper/sqldeveloper.sh
Icon=/home/<your_username>/Downloads/oracle/sqldeveloper/icon.png
Terminal=false
Type=Application
Categories=Development;

```

NOTE: put your your username inside `<your_username>`



### connecting 
connect to the database with your own `password` 

![SQL Developer](/images/oracle_db/SQL_dev.png)




## Connecting with *`DBeaver`*

Install `DBeaver` using package manager of your Linux distro

Use below configuration as below with your own `password`

![DBeaver](/images/oracle_db/DBeaver.png)
