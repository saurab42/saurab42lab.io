+++
title="About"
+++


Hello, I am Saurab Tharu. I am technology enthusiast, driven by a relentless curiosity for computer science.
I enjoy learning about computer stuff like making computer programs, understanding how the computer works inside.
I'm always excited to explore and discover new things in the world of computers.
